set nocompatible
set history=1000
set ruler
set laststatus=2
set showcmd
set wildmenu

set ttimeout
set ttimeoutlen=100

set display=truncate

map Q gq
sunmap Q

set mouse=a

set shiftwidth=4 smarttab
set expandtab
set tabstop=8 softtabstop=0

set incsearch

syntax on

" use current line as a command and replace line with command output
:noremap Q !!$SHELL<CR>

" Enable file type detection.
" Use the default filetype settings, so that mail gets 'tw' set to 72,
" 'cindent' is on in C files, etc.
" Also load indent files, to automatically do language-dependent indenting.
" Revert with ":filetype off".
filetype plugin indent on

" Put these in an autocmd group, so that you can revert them with:
" ":augroup vimStartup | exe 'au!' | augroup END"
augroup vimStartup
    au!

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid, when inside an event handler
    " (happens when dropping a file on gvim) and for a commit message (it's
    " likely a different one than last time).
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif

augroup END

set background=dark
colorscheme PaperColor

set path+=**
set wildmenu

let mapleader = ","

nnoremap <F3> :set hlsearch!<CR>
set hlsearch

" setup netrw like NerdTree
let g:netrw_banner = 0
let g:netrw_liststyle = 3
" let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
" augroup ProjectDrawer
"     autocmd!
"     autocmd VimEnter * :Vexplore
" augroup END

