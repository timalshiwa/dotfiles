#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '

HISTSIZE=9000
HISTFILESIZE=$HISTSIZE
# Avoid duplicates
HISTCONTROL=ignoreboth
# When the shell exits, append to the history file
shopt -s histappend

# After each command, append to the history file
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
# PROMPT_COMMAND="${PROMPT_COMMAND:+PROMPT_COMMAND$'\n'}history -a"

set -o vi

alias chmox='chmod +x'
alias path='echo -e ${PATH//:/\\n}'

source /usr/share/bash-completion/completions/git

export EDITOR=vim
export PATH=$HOME/.local/bin:$PATH

