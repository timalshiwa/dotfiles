stow bash

stow git

stow vim
# rebuild helptags for all modules. 'helptags ALL' stop on error in love2d
for D in $(find $HOME/.vim/pack -type d -name doc); do vim -u NONE -c "helptags $D" -c q; done

stow tmux
